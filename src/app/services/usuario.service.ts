import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})

export class UsuarioService {
url= 'https://www.thecocktaildb.com/api/json/v1/1/list.php?i=list';

  constructor(public  http:HttpClient) { }


  obtenerValor(): Observable<any>{
    return this.http.get<any>(this.url);
  }

}
